package dao;

import connection.ConnectionFactory;
import model.Product;


public class ProductDAO extends AbstractDAO<Product> {
	
	public ProductDAO() {
		super();
	}

	private String createInsertQuery() {
		StringBuilder sb = new StringBuilder();
		sb.append("INSERT INTO product");
		sb.append(" (barcode,denumire,stock,value)");
		sb.append("VALUES (?,?,?,?)");
		return sb.toString();

	}

}

