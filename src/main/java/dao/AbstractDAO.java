package dao;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import connection.ConnectionFactory;

public class AbstractDAO<T> {
	protected static final Logger LOGGER = Logger.getLogger(AbstractDAO.class.getName());

	private final Class<T> type;

	@SuppressWarnings("unchecked")
	public AbstractDAO() {
		this.type = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];

	}

	private String createSelectQuery() {
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT ");
		sb.append(" * ");
		sb.append(" FROM ");
		sb.append(type.getSimpleName());
		sb.append(" WHERE " + type.getDeclaredFields()[0].getName() + " =?");
		
		return sb.toString();
	}
	
	private String createSelectQueryFindAll() {
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT ");
		sb.append(" * ");
		sb.append(" FROM ");
		sb.append(type.getSimpleName());
		
		return sb.toString();
	}
	
	private String createInsertQuery() {
		StringBuilder sb = new StringBuilder();
		sb.append("INSERT INTO ");
		sb.append(type.getSimpleName());
		sb.append(" ( ");
		
		for (Field field : type.getDeclaredFields()) {
			field.setAccessible(true);
			Object value = null;
			try {
				value = field.getName();
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			}
			sb.append(value + " , ");
		}
		
		sb.deleteCharAt(sb.length() - 2 );
		sb.append( ") ");
		sb.append(" VALUES ( ");
		
		for (Field field : type.getDeclaredFields()) {
			field.setAccessible(true);
			sb.append(" ?, ");
		}
		sb.deleteCharAt(sb.length() - 2 );
		sb.append( ") ");	
		
		return sb.toString();
	}
	
	private String createUpdateQuery() {
		StringBuilder sb = new StringBuilder();
		sb.append("UPDATE ");
		sb.append(type.getSimpleName());
		sb.append(" SET ");
		
		for (Field field : type.getDeclaredFields()) {
			field.setAccessible(true);
			try {
				sb.append(field.getName() + "= ?,");
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			}
			
		}
		sb.deleteCharAt(sb.length() - 1 );
		sb.append(" WHERE " + type.getDeclaredFields()[0].getName() + " =?");
		return sb.toString();
	}
	
	private String createDeleteQuery() {
		StringBuilder sb = new StringBuilder();
		sb.append("DELETE FROM ");
		sb.append(type.getSimpleName());
		sb.append(" WHERE " + type.getDeclaredFields()[0].getName() + " =?");
		
		return sb.toString();
	}

	public List<T> findAll() {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		String query = createSelectQueryFindAll();
		try {
			connection = ConnectionFactory.getConnection();
			statement = connection.prepareStatement(query);
			resultSet = statement.executeQuery();

			return (List<T>) createObjects(resultSet);
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, type.getName() + "DAO:findById " + e.getMessage());
		} finally {
			ConnectionFactory.close(resultSet);
			ConnectionFactory.close(statement);
			ConnectionFactory.close(connection);
		}
		return null;
	}

	public T findById(int id) {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		String query = createSelectQuery();
		try {
			connection = ConnectionFactory.getConnection();
			statement = connection.prepareStatement(query);
			statement.setInt(1, id);
			resultSet = statement.executeQuery();

			return createObjects(resultSet).get(0);
		} catch (Exception e) {
			
		} finally {
			ConnectionFactory.close(resultSet);
			ConnectionFactory.close(statement);
			ConnectionFactory.close(connection);
		}
		return null;
	}

	protected List<T> createObjects(ResultSet resultSet) {
		List<T> list = new ArrayList<T>();

		try {
			while (resultSet.next()) {
				T instance = type.getDeclaredConstructor().newInstance();
				
				for (Field field : type.getDeclaredFields()) {
					Object value = resultSet.getObject(field.getName());
					PropertyDescriptor propertyDescriptor = new PropertyDescriptor(field.getName(), type);
					Method method = propertyDescriptor.getWriteMethod();
					method.invoke(instance, value);
				}
				list.add(instance);
			}
			
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (IntrospectionException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		}
		return list;
	}

	public T insert(T t) {
		Connection connection = null;
		PreparedStatement statement = null;
		String query = createInsertQuery();
		
		try {
			connection = ConnectionFactory.getConnection();
			statement = connection.prepareStatement(query);
			int index = 1;
			
			for(Field field : type.getDeclaredFields()) {
				field.setAccessible(true);
				Object value = null;
				try {
					value = field.get(t);
				} catch (IllegalArgumentException e) {
				} catch (IllegalAccessException e) {
				}
				
				if(field.getType().getSimpleName().equals("int")) {
					statement.setInt(index, Integer.parseInt(value.toString()));
					index++;
				}
				else if (field.getType().getSimpleName().equals("String")) {
					statement.setString(index, value.toString());
					index++;
				}else if (field.getType().getSimpleName().equals("float")) {
					statement.setFloat(index, Float.parseFloat(value.toString()));
					index++;
				}
			}
			
			statement.executeUpdate();

		
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, type.getName() + "DAO:insertObject " + e.getMessage());
		} finally {
			ConnectionFactory.close(statement);
			ConnectionFactory.close(connection);
		}
		return t;
	}

	public T update(T t, int id) {
		Connection connection = null;
		PreparedStatement statement = null;
		String query = createUpdateQuery();
		
		try {
			connection = ConnectionFactory.getConnection();
			statement = connection.prepareStatement(query);
			int index = 1;
			for(Field field : type.getDeclaredFields()) {
				field.setAccessible(true);
				Object value = null;
				try {
					value = field.get(t);
				} catch (IllegalArgumentException e) {
				} catch (IllegalAccessException e) {
				}
				
				if(field.getType().getSimpleName().equals("int")) {
					statement.setInt(index, Integer.parseInt(value.toString()));
					index++;
				}
				else if (field.getType().getSimpleName().equals("String")) {
					statement.setString(index, value.toString());
					index++;
				}else if (field.getType().getSimpleName().equals("float")) {
					statement.setFloat(index, Float.parseFloat(value.toString()));
					index++;
				}
			}
			statement.setInt(index, id);
			statement.executeUpdate();

		
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, type.getName() + "DAO:update " + e.getMessage());
		} finally {
			ConnectionFactory.close(statement);
			ConnectionFactory.close(connection);
		}
		return t;
	}
	
	public void delete(int id) {
		Connection connection = null;
		PreparedStatement statement = null;
		String query = createDeleteQuery();
		
		try {
			connection = ConnectionFactory.getConnection();
			statement = connection.prepareStatement(query);
			
			statement.setInt(1, id);
			statement.executeUpdate();

		
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, type.getName() + "DAO:update " + e.getMessage());
		} finally {
			ConnectionFactory.close(statement);
			ConnectionFactory.close(connection);
		}
	}
	
}
