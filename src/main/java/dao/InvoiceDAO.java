package dao;

import model.Invoice;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import connection.ConnectionFactory;

public class InvoiceDAO extends AbstractDAO<Invoice>{
	
	public InvoiceDAO() {
		super();
	}
	
	private String createSelectQueryOrder() {
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT ");
		sb.append(" * ");
		sb.append(" FROM ");
		sb.append("  invoice ");
		sb.append(" WHERE  clientId =?");
		
		return sb.toString();
	}
	
	private String createDeleteShopCartQuery() {
		StringBuilder sb = new StringBuilder();
		sb.append("DELETE FROM invoice ");
		sb.append(" WHERE clientId =?");
		
		return sb.toString();
	}
	
	public List<Invoice> findAllID(int id) {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		String query = createSelectQueryOrder();
		try {
			connection = ConnectionFactory.getConnection();
			statement = connection.prepareStatement(query);
			statement.setInt(1, id);
			resultSet = statement.executeQuery();

			return (List<Invoice>) createObjects(resultSet);
		} catch (Exception e) {
		
		} finally {
			ConnectionFactory.close(resultSet);
			ConnectionFactory.close(statement);
			ConnectionFactory.close(connection);
		}
		return null;
	}
	
	public Invoice findbyClientId(int id) {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		String query = createSelectQueryOrder();
		try {
			connection = ConnectionFactory.getConnection();
			statement = connection.prepareStatement(query);
			statement.setInt(1, id);
			resultSet = statement.executeQuery();

			return createObjects(resultSet).get(0);
		} catch (Exception e) {
		
		} finally {
			ConnectionFactory.close(resultSet);
			ConnectionFactory.close(statement);
			ConnectionFactory.close(connection);
		}
		return null;
	}
	
	public void deleteShopCart(int id) {
		Connection connection = null;
		PreparedStatement statement = null;
		String query = createDeleteShopCartQuery();
		
		try {
			connection = ConnectionFactory.getConnection();
			statement = connection.prepareStatement(query);
			
			statement.setInt(1, id);
			statement.executeUpdate();

		
		} catch (SQLException e) {
		
		} finally {
			ConnectionFactory.close(statement);
			ConnectionFactory.close(connection);
		}
	}
	
}
