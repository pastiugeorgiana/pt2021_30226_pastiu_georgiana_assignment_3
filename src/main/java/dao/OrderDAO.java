package dao;

import model.Orderr;

public class OrderDAO extends AbstractDAO <Orderr>{

	public OrderDAO() {
		super();
	}

	private String createInsertQuery() {
		StringBuilder sb = new StringBuilder();
		sb.append("INSERT INTO ");
		sb.append("orderr");
		sb.append(" ( `Inumber`,`clientId`,`product`,`price`)");
		sb.append("VALUES (?,?,?,?)");
	return sb.toString();}

}
