package bll;

import java.util.List;

import dao.ProductDAO;
import model.Product;

public class ProductBll {
	
	ProductDAO productDAO;
	
	public ProductBll() {
		productDAO = new ProductDAO();
	}

	public List<Product> getProductsFromDatabase(){
		List<Product> list = productDAO.findAll();
		if(list!= null) {
			return list;
		}///////////////////////////////////
		list.add(new Product(0,"", 0, 0.0F));
		return list;
	}
	

	public boolean insertProduct(Product product) {
		Product productTest = productDAO.findById(product.getBarcode());
		if(productTest != null)
			return false;
		productDAO.insert(product);
		return true;
	}

	public boolean updateProduct(Product product) {
		Product productTest = productDAO.findById(product.getBarcode());
		if(productTest == null)
			return false;
		productDAO.update(product, product.getBarcode());
		return true;
	}
	

	public boolean deleteProduct(int id) {
		Product productTest = productDAO.findById(id);
		if(productTest == null)
			return false;
		productDAO.delete(id);
		return true;
	}
	

	public Product findProduct(int id) {
		Product productTest = productDAO.findById(id);
		return productTest;
	}
	
}
