package bll;

import java.io.IOException;
import java.util.List;

import dao.ClientDAO;
import dao.ProductDAO;
import dao.InvoiceDAO;
import model.Client;
import model.Orderr;
import model.Product;
import model.Invoice;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDType1Font;

public class InvoiceBll {


	InvoiceDAO invoiceDAO;
	public InvoiceBll() {
		invoiceDAO = new InvoiceDAO();
	}


	public List<Invoice> getShopCartsFromDatabase(){
		List<Invoice> list = invoiceDAO.findAll();
		if(list.size() !=0 )
			return list;
		list.add(new Invoice(0,0,0));
		return list;
	}
	

	public int insertShopCart(Invoice shop) {
		ClientDAO clientDAO = new ClientDAO();
		Client clientTest = clientDAO.findById(shop.getClientId());
		if(clientTest == null)
			return -1;
		ProductDAO productDAO = new ProductDAO();
		Product product = productDAO.findById(shop.getItem());
		if(product == null)
			return -2;
		if(product.getStock() <=0)
			return -3;
		if(product.getStock() < shop.getPieces())
			return -3;
		invoiceDAO.insert(shop);
		return 0;
	}
	

	public void deleteShopCart(int clientId) {
		invoiceDAO.deleteShopCart(clientId);
	}
	

	public int createOrderId(int clientId){
		Invoice invoiceTest = invoiceDAO.findbyClientId(clientId);
			if(invoiceTest == null)
				return -1;
		OrderBll orderBll = new OrderBll();
		ProductBll productBll = new ProductBll();
		List<Invoice> list = invoiceDAO.findAllID(clientId);
		String products ="";
		////////
		float price = 0;
		for(Invoice shop : list) {
			Product product = productBll.findProduct(shop.getItem());
			if(product.getStock() < shop.getPieces())
				return -2;
			productBll.updateProduct(new Product(product.getBarcode(), product.getDenumire(), product.getStock()-shop.getPieces(), product.getPrice()));
			products += shop.getItem() +" ";/////////////
			price += product.getPrice()*(float)shop.getPieces();
		}

		invoiceDAO.deleteShopCart(clientId);
		Orderr orderr = new Orderr(clientId, products ,price);

		orderBll.insertOrder(orderr);
		orderBll.numberOfOrders++;

		PDDocument document=new PDDocument();
		PDPage page = new PDPage();
		document.addPage(page);

		PDPageContentStream contentStream = null;

		try {
			contentStream = new PDPageContentStream(document, page);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		 
		try {
			contentStream.setFont(PDType1Font.COURIER, 12);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			contentStream.beginText();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			contentStream.showText(orderr.toString());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			contentStream.endText();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			contentStream.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 
		try {
			document.save("newOrder" + orderBll.numberOfOrders + ".pdf");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	try {
			document.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


		return 0;
	}



}
