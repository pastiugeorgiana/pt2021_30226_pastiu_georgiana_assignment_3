package bll;

import java.util.List;

import dao.ClientDAO;
import model.Client;

public class ClientBll {
	
	ClientDAO clientDAO;
	
	public ClientBll() {
		clientDAO = new ClientDAO();
	}

	public List<Client> getClientsFromDatabase(){
		List<Client> list = clientDAO.findAll();
		if(list.size() !=0 )
			return list;
		list.add(new Client(0,"", "", "", ""));
		return list;
	}
	

	public boolean insertClient(Client client) {
		Client clientTest = clientDAO.findById(client.getId());
		if(clientTest != null)
			return false;
		clientDAO.insert(client);
		return true;
	}
	

	public boolean updateClient(Client client) {
		Client clientTest = clientDAO.findById(client.getId());
		if(clientTest == null)
			return false;
		clientDAO.update(client, client.getId());
		return true;
	}

	public boolean deleteClient(int id) {
		Client clientTest = clientDAO.findById(id);
		if(clientTest == null)
			return false;
		clientDAO.delete(id);
		return true;
	}
	
}
