package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import bll.ClientBll;
import model.Client;
import view.ClientView;

public class ClientController {

	static ClientView view;
	static ClientBll clientBll = new ClientBll();
	
	public ClientController(ClientView view) {
		this.view = view;
		this.view.addButonInsertListener(new InsertListener());
		this.view.addButonUpdateListener(new UpdateListener());
		this.view.addButonDeleteListener(new DeleteListener());
		ClientController.createTable();
	}
	
	public static void createClient(String clientId, String name_, String _name, String address, String emailAddress) {
		Client client = new Client(Integer.parseInt(clientId), name_, _name, address, emailAddress);
		boolean test = clientBll.insertClient(client);
		if(!test) {
			view.showErrorMessageInsertExistAlready();
		}
		if(view.isTableOn()) {
			ClientController.createTable();
		}
	}
	
	public static void updateClient(String clientId, String name_, String _name, String address, String emailAddress) {
		Client client = new Client(Integer.parseInt(clientId), name_, _name, address, emailAddress);
		boolean test = clientBll.updateClient(client);
		if(!test) {
			view.showErrorMessageeInvalidId();
		}
		if(view.isTableOn()) {
			ClientController.createTable();
		}
	}
	
	public static void deleteClient(String clientId) {
		boolean test = clientBll.deleteClient(Integer.parseInt(clientId));
		if(!test) {
			view.showErrorMessageeInvalidId();
		}
		if(view.isTableOn()) {
			ClientController.createTable();
		}
	}
	
	public static void createTable() {
		List<Client> list = clientBll.getClientsFromDatabase();
		view.setTableShowAll(list);
		view.updateView();
	}
	
	class InsertListener implements ActionListener{

		public void actionPerformed(ActionEvent e) {
			String clientId = "";
			String clientName = "";
			String clientAddress = "";
			String phoneNumber = "";
			String email = "";
			boolean parse = true;
			try {
				clientId = view.getClientFieldId();
				clientName = view.getClientFieldName();
				clientAddress = view.getClientFieldAddress();
				phoneNumber = view.getClientPhoneNumber();
				email = view.getClientEmailAddress();
			} catch(NumberFormatException nfex) {
			}
			if(clientId.equals("") || clientName.equals("") || clientAddress.equals("") || phoneNumber.equals("") || email.equals(""))
				view.showErrorMessageInsert();
			else {
				try {
					Integer.parseInt(clientId);
				}
				catch(Exception ex) {
					view.showErrorMessageeInvalidIdParse();
					parse = false;
				}
				if(parse)
					ClientController.createClient(clientId, clientName, clientAddress, phoneNumber, email);
			}
		}
	}
	
	class UpdateListener implements ActionListener{

		public void actionPerformed(ActionEvent e) {
			String clientId = "";
			String clientName = "";
			String clientPrename = "";
			String address = "";
			String email = "";
			boolean parse = true;
			try {
				clientId = view.getClientFieldId();
				clientName = view.getClientFieldName();
				clientPrename = view.getClientFieldAddress();
				address = view.getClientPhoneNumber();
				email = view.getClientEmailAddress();
			} catch(NumberFormatException nfex) {
			}
			if(clientId.equals("") || clientName.equals("") || clientPrename.equals("") || address.equals("") || email.equals(""))
				view.showErrorMessageUpdate();
			else{
				try {
					Integer.parseInt(clientId);
				}
				catch(Exception ex) {
					view.showErrorMessageeInvalidIdParse();
					parse = false;
				}
				if(parse)
					ClientController.updateClient(clientId, clientName, clientPrename, address, email);
			}
		}
	
	}
	
	class DeleteListener implements ActionListener{

		public void actionPerformed(ActionEvent e) {
			String clientId = "";
			
			try {
				clientId = view.getClientFieldId();
			} catch(NumberFormatException nfex) {
			}
			if(clientId.equals(""))
				view.showErrorMessageDelete();
			else
				ClientController.deleteClient(clientId);
		}
	
	}
	

	
}
