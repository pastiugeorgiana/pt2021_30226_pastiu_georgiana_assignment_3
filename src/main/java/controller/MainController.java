package controller;

import view.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MainController {
	
	MainView view;
	static ClientView clientView = new ClientView();
	static ProductView productView = new ProductView();
	static OrderView orderView = new OrderView();;
	static ClientController clientController;
	static OrderController orderController;
	static ProductController productController;
	
	public MainController(MainView view) {
		this.view = view;
		this.view.addButtonListener( new SelectedOption());
	}

	public static void execute(String option) {
		switch(option) {
		case "Client Operations": 
			clientController = new ClientController(clientView);
			productView.frame.setVisible(false);
			orderView.frame.setVisible(false);
			clientView.frame.setVisible(true);
			break;
		case "Product Operations":
			productController = new ProductController(productView);
			clientView.frame.setVisible(false);
			orderView.frame.setVisible(false);
			productView.frame.setVisible(true);
			break;
		case "Order Operations": 
			clientController = new ClientController(clientView);
			clientView.frame.setVisible(true);
			productController = new ProductController(productView);
			productView.frame.setVisible(true);
			orderController = new OrderController(orderView);
			orderView.frame.setVisible(true);
			break;
		}
	}
	
	class SelectedOption implements ActionListener{

		public void actionPerformed(ActionEvent e) {
			MainController.execute(view.getSelectedOption());
		}
		
	}
	
}
