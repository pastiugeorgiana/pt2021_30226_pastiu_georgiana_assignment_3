package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import bll.ProductBll;
import model.Product;
import view.ProductView;

public class ProductController {
	
	static ProductView view;
	static ProductBll productBll = new ProductBll();
	
	public ProductController(ProductView view) {
		this.view = view;
		this.view.addButonInsertListener(new InsertListener());
		this.view.addButonUpdateListener(new UpdateListener());
		this.view.addButonDeleteListener(new DeleteListener());
		ProductController.createTable();
	}
	//////////////////////////////////////////////
	public static void createProduct(String productId, String productName, String cantity, String price) {
		Product product = new Product(Integer.parseInt(productId), productName,  Integer.parseInt(cantity), Float.parseFloat(price));
		boolean test = productBll.insertProduct(product);
		if(!test) {
			view.showErrorMessageInsertExistAlready();
		}
		if(view.isTableOn()) {
			ProductController.createTable();
		}
	}
	///////////////////////////////////////////////
	public static void updateProduct(String productId, String productName, String cantity, String price) {
		Product product = new Product(Integer.parseInt(productId), productName,  Integer.parseInt(cantity), Float.parseFloat(price));
		boolean test = productBll.updateProduct(product);
		if(!test) {
			view.showErrorMessageeInvalidId();
		}
		if(view.isTableOn()) {
			ProductController.createTable();
		}
	}
	
	public static void deleteProduct(String productId) {
		boolean test = productBll.deleteProduct(Integer.parseInt(productId));
		if(!test) {
			view.showErrorMessageeInvalidId();
		}
		if(view.isTableOn()) {
			ProductController.createTable();
		}
	}
	
	public static void createTable() {
		List<Product> list = productBll.getProductsFromDatabase();
		view.setTableShowAll(list);
		view.updateView();
	}
	
	class InsertListener implements ActionListener{

		public void actionPerformed(ActionEvent e) {
			String productId = "";
			String productName = "";
			String productCantity = "";
			String productPrice = "";

			boolean parse = true;
			
			try {
				productId = view.getProductFieldId();
				productName = view.getProductFieldName();
				productCantity = view.getProductCantity();
				productPrice = view.getProductPrice();
				} catch(NumberFormatException nfex) {
			}
			if(productId.equals("") || productName.equals("") || productCantity.equals("") || productPrice.equals("") )
				view.showErrorMessageInsert();
			else{
				try {
					Integer.parseInt(productId);
					Integer.parseInt(productCantity);
					Float.parseFloat(productPrice);
				}
				catch(Exception ex) {
					view.showErrorMessageeInvalidIdParse();
					parse = false;
				}
				if(parse)
					ProductController.createProduct(productId, productName, productCantity, productPrice);
			}
		}
	
	}
	
	class UpdateListener implements ActionListener{

		public void actionPerformed(ActionEvent e) {
			String productId = "";
			String productName = "";
			String productCantity = "";
			String productPrice = "";

			
			try {
				productId = view.getProductFieldId();
				productName = view.getProductFieldName();
				productCantity = view.getProductCantity();
				productPrice = view.getProductPrice();

			} catch(NumberFormatException nfex) {
			}
			if(productId.equals("") || productName.equals("") || productCantity.equals("") || productPrice.equals("") )
				view.showErrorMessageInsert();
			else
				ProductController.updateProduct(productId, productName, productCantity, productPrice);
		}
	
	}
	
	class DeleteListener implements ActionListener{

		public void actionPerformed(ActionEvent e) {
			String productId = "";
			
			try {
				productId = view.getProductFieldId();
			} catch(NumberFormatException nfex) {
			}
			if(productId.equals(""))
				view.showErrorMessageDelete();
			else
				ProductController.deleteProduct(productId);
		}
	
	}
	

	
}
