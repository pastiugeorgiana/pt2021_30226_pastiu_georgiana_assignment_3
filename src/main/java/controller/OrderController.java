package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import bll.OrderBll;
import bll.InvoiceBll;
import model.Orderr;
import model.Invoice;
import view.OrderView;

public class OrderController {

	static OrderView view;
	static InvoiceBll shopBll = new InvoiceBll();
	static OrderBll orderBll = new OrderBll();
	
	public OrderController(OrderView view) {
		this.view = view;
		this.view.addActionListenerAddToShopCart(new InsertInvoiceListener());
		this.view.addActionListenerShowOrders(new ShowAllOrdersListener());
		this.view.addActionListenerCreateOrder(new CreateOrderListener());
		this.view.addActionListenerShowShopCarts(new ShowAllListener());
		OrderController.createTable();
	}
	
	public static void createShopCart(String clientId , String productId, String cantity) { 
		int idClient = Integer.parseInt(clientId);
		int idProdus = Integer.parseInt(productId);
		int cantitate = Integer.parseInt(cantity);
		Invoice shop = new Invoice(idProdus, cantitate, idClient);
		int insert = shopBll.insertShopCart(shop);
		switch(insert) {
		case 0 :
			if(view.isTableOn()) {
				OrderController.createTable();
			}
			break;
		case -1: view.showErrorMessageeClientIndisponibil(); break;
		case -2 : view.showErrorMessageeProdusIndisponibil(); break;
		case -3 : view.showErrorMessageeStocIndisponibil(); break;
		}
		
	}
	
	public static void deleteShopCart(String id) {
		int clientId = Integer.parseInt(id);
		shopBll.deleteShopCart(clientId);
		if(view.isTableOn()) {
			OrderController.createTableOrders();
		}
	}
	
	public static void addFromShopToOrder(String id) {
		int clientId = Integer.parseInt(id);
		int test = shopBll.createOrderId(clientId);
		switch (test) {
		case 0:
			if(view.isTableOn()) {
				OrderController.createTable();
			}
			break;
		case -1:
			view.showErrorMessageeClientIndisponibilToOrder();
			break;
		case -2:
			view.showErrorMessageeStocIndisponibil();
			break;
		}
	}
	
	public static void createTable() {
		List<Invoice> list = shopBll.getShopCartsFromDatabase();
		view.setTableShowAllShop(list);
		view.updateView();
	}
	
	public static void createTableOrders() {
		List<Orderr> list = orderBll.getOrdersFromDatabase();
		view.setTableShowAll(list);
		view.updateView();
	}
	
	class InsertInvoiceListener implements ActionListener{

		public void actionPerformed(ActionEvent e) {
			String clientId = "";
			String productId = "";
			String cantity = "";
			
			boolean parse = true;
			try {
				clientId = view.getClientIdText();
				productId = view.getProductIdText();
				cantity = view.getProductCantityText();
			
			} catch(NumberFormatException nfex) {
			}
			if(clientId.equals("") || productId.equals("") || cantity.equals(""))
				view.showErrorMessageInsert();
			else {
				try {
					Integer.parseInt(clientId);
					Integer.parseInt(productId);
					Integer.parseInt(cantity);
				}
				catch(Exception ex) {
					view.showErrorMessageeInvalidIdParse();
					parse = false;
				}
				if(parse)
					OrderController.createShopCart(clientId, productId, cantity);
			}
		}
	}
	
	class ShowAllListener implements ActionListener{

		public void actionPerformed(ActionEvent e) {
			OrderController.createTable();
		}
		
	}
	
	class ShowAllOrdersListener implements ActionListener{

		public void actionPerformed(ActionEvent e) {
			OrderController.createTableOrders();
		}
		
	}
	
	class CreateOrderListener implements ActionListener{

		public void actionPerformed(ActionEvent e) {
			String clientId = "";
			boolean parse = true;
			try {
				clientId = view.getClientIdOrder();
			} catch(NumberFormatException nfex) {
			}
			if(clientId.equals(""))
				view.showErrorMessageInsert();
			else {
				try {
					Integer.parseInt(clientId);
				}
				catch(Exception ex) {
					view.showErrorMessageeInvalidIdParse();
					parse = false;
				}
				if(parse)
					OrderController.addFromShopToOrder(clientId);
					OrderController.deleteShopCart(clientId);
			}
			
		}
		
	}
	
}
