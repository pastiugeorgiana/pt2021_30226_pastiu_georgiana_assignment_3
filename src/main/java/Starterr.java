import controller.MainController;
import view.MainView;

import java.io.IOException;

public class Starterr
{
    public static void main( String[] args ) throws IOException {
    	MainView view = new MainView();
    	MainController controller = new MainController(view);
    	view.setVisible(true);


	}
}
