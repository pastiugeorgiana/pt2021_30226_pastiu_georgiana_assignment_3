package view;

import java.lang.reflect.Field;
import java.util.List;

import javax.swing.JTable;

public class AbstractView<T> {

	private JTable table;
	private String[][] data;
	private String[] columns;
	
	public  JTable createTable(List<T> objects) {
			
			int fieldsNumber = 0;
			for(Field field : objects.get(0).getClass().getDeclaredFields()) {
				fieldsNumber++;
			}

			columns = new String[fieldsNumber];
			data = new String[objects.size()][fieldsNumber];
			
			fieldsNumber= 0;
			for(Field field : objects.get(0).getClass().getDeclaredFields()) {
				columns[fieldsNumber++] = field.getName().toString();
			}
			
			for(int index = 0; index < objects.size(); index++) {
				int index2 = 0;
				for(Field field : objects.get(index).getClass().getDeclaredFields()) {
					field.setAccessible(true);
					try {
						if(field.get(objects.get(index)).toString().equals("0"))
							data[index][index2] = "0";
						else
							data[index][index2] = field.get(objects.get(index)).toString();
					} catch (IllegalArgumentException e) {
						
						e.printStackTrace();
					} catch (IllegalAccessException e) {
						e.printStackTrace();
					}
					index2++;
				}
			}
			
			table = new JTable(data, columns);
			table.setBounds(30,30,150,100);
			return table;
		}
	
}
