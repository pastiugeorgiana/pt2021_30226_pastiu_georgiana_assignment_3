package view;

import java.awt.Dimension;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

import model.Orderr;
import model.Invoice;

public class OrderView extends AbstractView {
	
	public JFrame frame = new JFrame();
	JLabel labelOrder = new JLabel("Order Operations");
	JLabel labelProductId = new JLabel("Product id:");
	JLabel labelProductR = new JLabel("Product pieces requested:");
	JLabel labelClientId = new JLabel("Client id:");
	JLabel orderrId = new JLabel("...for the following client id :");
	JTextField fieldProductId = new JTextField(20);
	JTextField fieldStock = new JTextField(20);
	JTextField fieldClientId = new JTextField(20);
	JTextField orderClientId = new JTextField(20);
	static JTable tableAllClients = new JTable();
	JPanel content2 = new JPanel();
	JButton buttonCreateOrder = new JButton("Place order :) ...");
	JButton buttonRequest = new JButton("Request items");
	JButton buttonShowOrders = new JButton("Show placed orders");
	JButton buttonShowRequest= new JButton("Show requested items");
	JScrollPane sp = new JScrollPane();
	boolean tableOn = false;
	
	public OrderView() {
		
		frame.setLocation(450, 600);
		frame.setSize(1300,300);
		
		JPanel mainContent = new JPanel();
		JPanel content0 = new JPanel();
		JPanel content1 = new JPanel();
		JPanel contentLabel = new JPanel();
		JPanel contentId = new JPanel();
		JPanel contentProductId = new JPanel();
		JPanel contentCantity = new JPanel();
		JPanel contentCreateButon = new JPanel();
		JPanel contentOrder = new JPanel();
		
		mainContent.setLayout(new BoxLayout(mainContent, BoxLayout.X_AXIS));
		content0.setLayout(new BoxLayout(content0,BoxLayout.Y_AXIS));
		content1.setLayout(new BoxLayout(content1,BoxLayout.Y_AXIS));
		content2.setLayout(new BoxLayout(content2,BoxLayout.Y_AXIS));
		
		contentLabel.add(labelOrder);
		
		contentId.add(labelClientId);
		contentId.add(fieldClientId);
		
		contentProductId.add(labelProductId);
		contentProductId.add(fieldProductId);
		
		contentCantity.add(labelProductR);
		contentCantity.add(fieldStock);
		contentOrder.add(orderrId);
		contentCreateButon.add(buttonRequest);
		

		contentOrder.add(orderClientId);
		
		content0.add(contentLabel);
		content0.add(contentId);
		content0.add(contentProductId);
		content0.add(contentCantity);
		content0.add(contentCreateButon);

		content1.add(Box.createRigidArea(new Dimension(10,10)));
		content1.add(buttonCreateOrder);
		content1.add(Box.createRigidArea(new Dimension(10,10)));
		content1.add(contentOrder);

		content1.add(Box.createRigidArea(new Dimension(10,10)));
	content1.add(buttonShowRequest);
		content1.add(Box.createRigidArea(new Dimension(10,10)));
		content1.add(Box.createRigidArea(new Dimension(10,10)));
		content1.add(buttonShowOrders);
		content0.setBorder(new TitledBorder(new EtchedBorder(), " "));
		content1.setBorder(new TitledBorder(new EtchedBorder(), " "));
		content2.setBorder(new TitledBorder(new EtchedBorder(), " "));
		
		mainContent.add(content0);
		mainContent.add(content1);
		mainContent.add(content2);
		
		frame.setContentPane(mainContent);
		frame.setTitle("Order Operations");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	public String getClientIdText() {
		return fieldClientId.getText();
	}
	
	public String getProductIdText() {
		return fieldProductId.getText();
	}
	
	public String getProductCantityText() {
		return fieldStock.getText();
	}
	
	public String getClientIdOrder() {
		return orderClientId.getText();
	}
	
	public boolean isTableOn() {
		return tableOn;
	}
	
	public void addActionListenerCreateOrder(ActionListener e) {
		buttonCreateOrder.addActionListener(e);
	}
	
	public void addActionListenerShowOrders(ActionListener e) {
		buttonShowOrders.addActionListener(e);
	}
	
	public void addActionListenerAddToShopCart(ActionListener e) {
		buttonRequest.addActionListener(e);
	}
	
	public void addActionListenerShowShopCarts(ActionListener e) {
		buttonShowRequest.addActionListener(e);
	}
	
	public void updateView() {
		SwingUtilities.updateComponentTreeUI(frame);
	}
	
	public void setTableShowAll(List<Orderr> list) {
		tableOn = true;
		content2.removeAll();
		tableAllClients = this.createTable(list);
		sp = new JScrollPane(tableAllClients); 
		sp.setBounds(tableAllClients.getBounds());
		content2.add(sp);
	}
	
	public void setTableShowAllShop(List<Invoice> list) {
		tableOn = true;
		content2.removeAll();
		tableAllClients = this.createTable(list);
		sp = new JScrollPane(tableAllClients); 
		sp.setBounds(tableAllClients.getBounds());
		content2.add(sp);
	}
	
	public void showErrorMessageInsert() {
		JOptionPane.showMessageDialog(frame,"Please fill all the fields", "Error", JOptionPane.ERROR_MESSAGE);
	}

	public void showErrorMessageeInvalidIdParse() {
		JOptionPane.showMessageDialog(frame,"id and stock are of type int and price is float", "Error", JOptionPane.ERROR_MESSAGE);
	}
	
	public void showErrorMessageeStocIndisponibil() {
		JOptionPane.showMessageDialog(frame,"stock overflow ;) ", "Error", JOptionPane.ERROR_MESSAGE);
	}
	
	public void showErrorMessageeClientIndisponibil() {
		JOptionPane.showMessageDialog(frame,"invalid client id", "Error", JOptionPane.ERROR_MESSAGE);
	}
	
	public void showErrorMessageeClientIndisponibilToOrder() {
		JOptionPane.showMessageDialog(frame,"this client made no product requests", "Error", JOptionPane.ERROR_MESSAGE);
	}
	
	public void showErrorMessageeProdusIndisponibil() {
		JOptionPane.showMessageDialog(frame,"invalid product id", "Error", JOptionPane.ERROR_MESSAGE);
	}
	
}
