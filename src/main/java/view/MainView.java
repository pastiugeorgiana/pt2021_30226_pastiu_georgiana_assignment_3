package view;

import java.awt.event.ActionListener;

import javax.swing.*;

public class MainView extends JFrame {
	
	private String[] options = { "Client Operations", "Product Operations", "Order Operations"};
	private JComboBox selectClientOrProduct =  new JComboBox(options);

	private JButton showButton = new JButton("Open");
	
	public MainView() {
		
		this.setLocation(0, 425);
		this.setSize(450,150);
		
		JPanel mainContent = new JPanel();
		JPanel content0 = new JPanel();
		JPanel content1 = new JPanel();
		mainContent.setLayout(new BoxLayout(mainContent, BoxLayout.Y_AXIS));

		content0.add(selectClientOrProduct);

		mainContent.add(content0);
		content1.add(showButton);
		mainContent.add(content1);
		this.setContentPane(mainContent);
		this.setTitle("WAREHOUSE");
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
	}
	
	public String getSelectedOption() {
		return selectClientOrProduct.getSelectedItem().toString();
	}

	public void addButtonListener(ActionListener e) {
		showButton.addActionListener(e);
	}

}
