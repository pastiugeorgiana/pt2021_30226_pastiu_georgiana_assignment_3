package view;

import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

import model.Product;

public class ProductView extends AbstractView{
	
	public JFrame frame = new JFrame();
	JButton butonInsert = new JButton("Insert");
	JButton butonUpdate = new JButton("Update");
	JButton butonDelete = new JButton("Delete");
	JLabel labelProduct = new JLabel("Product Operations");
	JLabel labelProducttId = new JLabel("Product id:");
	JLabel labelProductName = new JLabel("Product name:");
	JLabel labelProductCantity = new JLabel("Product stock:");
	JLabel labelProductPrice = new JLabel("Product price:");
	JTextField fieldId = new JTextField(20);
	JTextField fieldName = new JTextField(20);
	JTextField fieldStock = new JTextField(20);
	JTextField fieldPrice = new JTextField(20);
	static JTable tableAllProducts = new JTable();

	JScrollPane sp = new JScrollPane();
	JPanel content2 = new JPanel();
	boolean tableOn = false;
	
	public ProductView() {
		
		frame.setLocation(450, 300);
		frame.setSize(1300,300);
		
		JPanel mainContent = new JPanel();
		JPanel content0 = new JPanel();
		JPanel content1 = new JPanel();
		JPanel contentLabel = new JPanel();
		JPanel contentButoane = new JPanel();
		JPanel contentId = new JPanel();
		JPanel contentName = new JPanel();
		JPanel contentAddress = new JPanel();
		JPanel contentPhone = new JPanel();
		JPanel contentEmail = new JPanel();
		JPanel contentbuton1 = new JPanel();
		JPanel contentbuton2 = new JPanel();
		JPanel contentbuton3 = new JPanel();
		JPanel contentbuton4 = new JPanel();
		
		mainContent.setLayout(new BoxLayout(mainContent, BoxLayout.X_AXIS));
		content0.setLayout(new BoxLayout(content0,BoxLayout.Y_AXIS));
		content1.setLayout(new BoxLayout(content1,BoxLayout.Y_AXIS));
		content2.setLayout(new BoxLayout(content2,BoxLayout.Y_AXIS));
		
		contentLabel.add(labelProduct);

		contentbuton1.add(butonInsert);
		contentbuton2.add(butonUpdate);
		contentbuton3.add(butonDelete);
		contentId.add(labelProducttId);
		contentId.add(fieldId);
		
		contentName.add(labelProductName);
		contentName.add(fieldName);
		
		contentAddress.add(labelProductCantity);
		contentAddress.add(fieldStock);
		
		contentPhone.add(labelProductPrice);
		contentPhone.add(fieldPrice);
		

		
		content0.add(contentLabel);
		content0.add(contentId);
		content0.add(contentName);
		content0.add(contentAddress);
		content0.add(contentPhone);
		content0.add(contentEmail);
		
		content1.add(contentButoane);
		content1.add(contentbuton1);
		content1.add(contentbuton2);
		content1.add(contentbuton3);
		content1.add(contentbuton4);
		
		content0.setBorder(new TitledBorder(new EtchedBorder(), " "));
		content1.setBorder(new TitledBorder(new EtchedBorder(), " "));
		content2.setBorder(new TitledBorder(new EtchedBorder(), " "));
		
		mainContent.add(content0);
		mainContent.add(content1);
		mainContent.add(content2);
		
		frame.setContentPane(mainContent);
		frame.setTitle("Product Operations");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
	}
	
	public String getProductFieldId() {
		return fieldId.getText();
	}
	
	public String getProductFieldName() {
		return fieldName.getText();
	}
	
	public String getProductCantity() {
		return fieldStock.getText();
	}
	
	public String getProductPrice() {
		return fieldPrice.getText();
	}
	

	public boolean isTableOn() {
		return tableOn;
	}
	
	public void updateView() {
		SwingUtilities.updateComponentTreeUI(frame);
	}
	
	public void setTableShowAll(List<Product> list) {
		tableOn = true;
		content2.removeAll();
		tableAllProducts = this.createTable(list);
		sp = new JScrollPane(tableAllProducts); 
		sp.setBounds(tableAllProducts.getBounds());
		content2.add(sp);
	}
	
	public void addButonInsertListener(ActionListener e) {
		butonInsert.addActionListener(e);
	}
	
	public void addButonUpdateListener(ActionListener e) {
		butonUpdate.addActionListener(e);
	}
	
	public void addButonDeleteListener(ActionListener e) {
		butonDelete.addActionListener(e);
	}

	public void showErrorMessageInsert() {
		JOptionPane.showMessageDialog(frame,"Please fill all the fields", "Error", JOptionPane.ERROR_MESSAGE);
	}
	
	public void showErrorMessageUpdate() {
		JOptionPane.showMessageDialog(frame,"Please fill all the fields", "Error", JOptionPane.ERROR_MESSAGE);
	}
	
	public void showErrorMessageDelete() {
		JOptionPane.showMessageDialog(frame," Please fill the product id field to delete", "Error", JOptionPane.ERROR_MESSAGE);
	}
	
	public void showErrorMessageInsertExistAlready() {
		JOptionPane.showMessageDialog(frame,"Primary key violated", "Error", JOptionPane.ERROR_MESSAGE);
	}
	
	public void showErrorMessageeInvalidId() {
		JOptionPane.showMessageDialog(frame,"the product id does not exist", "Error", JOptionPane.ERROR_MESSAGE);
	}
	
	public void showErrorMessageeInvalidIdParse() {
		JOptionPane.showMessageDialog(frame,"id and stock are of type int and price is float", "Error", JOptionPane.ERROR_MESSAGE);
	}
	
}
