package view;

import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.*;

import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

import model.Client;

public class ClientView extends AbstractView{
	
	JLabel labelClient = new JLabel("Client operations");
	JLabel labelClientId = new JLabel("Client id:");
	JLabel labelClientName = new JLabel("Client name:");
	JLabel labelClientAddress = new JLabel("Client first name:");
	JLabel labelClientPhoneNumber = new JLabel("Client address:");
	JLabel labelClientEmail = new JLabel("Client email address:");
	JTextField fieldId = new JTextField(20);
	JTextField fieldName = new JTextField(20);
	JTextField fieldAddress = new JTextField(20);
	JTextField fieldPhone = new JTextField(20);
	JTextField fieldEmail = new JTextField(20);
	static JTable tableAllClients = new JTable();
	JScrollPane sp = new JScrollPane();
	JPanel content2 = new JPanel();
	public JFrame frame = new JFrame();
	JButton butonInsert = new JButton("Insert");
	JButton butonUpdate = new JButton("Update");
	JButton butonDelete = new JButton("Delete");
	boolean tableOn = false;
	
	public ClientView() {
		
		
		frame.setLocation(450, 0);
		frame.setSize(1300,300);
		
		JPanel mainContent = new JPanel();
		JPanel content0 = new JPanel();
		JPanel content1 = new JPanel();
		JPanel contentLabel = new JPanel();
		JPanel contentButoane = new JPanel();
		JPanel contentId = new JPanel();
		JPanel contentName = new JPanel();
		JPanel contentAddress = new JPanel();
		JPanel contentPhone = new JPanel();
		JPanel contentEmail = new JPanel();
		JPanel contentbuton1 = new JPanel();
		JPanel contentbuton2 = new JPanel();
		JPanel contentbuton3 = new JPanel();
		JPanel contentbuton4 = new JPanel();
		
		
		mainContent.setLayout(new BoxLayout(mainContent, BoxLayout.X_AXIS));
		content0.setLayout(new BoxLayout(content0,BoxLayout.Y_AXIS));
		content1.setLayout(new BoxLayout(content1,BoxLayout.Y_AXIS));
		content2.setLayout(new BoxLayout(content2,BoxLayout.Y_AXIS));
		
		contentLabel.add(labelClient);
		

		contentbuton1.add(butonInsert);
		contentbuton2.add(butonUpdate);
		contentbuton3.add(butonDelete);

		contentId.add(labelClientId);
		contentId.add(fieldId);
		
		contentName.add(labelClientName);
		contentName.add(fieldName);
		
		contentAddress.add(labelClientAddress);
		contentAddress.add(fieldAddress);
		
		contentPhone.add(labelClientPhoneNumber);
		contentPhone.add(fieldPhone);
		
		contentEmail.add(labelClientEmail);
		contentEmail.add(fieldEmail);
		
		content0.add(contentLabel);
		content0.add(contentId);
		content0.add(contentName);
		content0.add(contentAddress);
		content0.add(contentPhone);
		content0.add(contentEmail);
		
		content1.add(contentButoane);
		content1.add(contentbuton1);
		content1.add(contentbuton2);
		content1.add(contentbuton3);
		content1.add(contentbuton4);
		
		content0.setBorder(new TitledBorder(new EtchedBorder(), " "));
		content1.setBorder(new TitledBorder(new EtchedBorder(), " "));
		content2.setBorder(new TitledBorder(new EtchedBorder(), " "));
		
		mainContent.add(content0);
		mainContent.add(content1);
		mainContent.add(content2);
		
		frame.setContentPane(mainContent);
		frame.setTitle(" Client Operations");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
	}
	
	public String getClientFieldId() {
		return fieldId.getText();
	}
	
	public String getClientFieldName() {
		return fieldName.getText();
	}
	
	public String getClientFieldAddress() {
		return fieldAddress.getText();
	}
	
	public String getClientPhoneNumber() {
		return fieldPhone.getText();
	}
	
	public String getClientEmailAddress() {
		return fieldEmail.getText();
	}
	
	public boolean isTableOn() {
		return tableOn;
	}
	
	public void updateView() {
		SwingUtilities.updateComponentTreeUI(frame);
	}
	
	public void setTableShowAll(List<Client> list) {
		tableOn = true;
		content2.removeAll();
		tableAllClients = this.createTable(list);
		sp = new JScrollPane(tableAllClients); 
		sp.setBounds(tableAllClients.getBounds());
		content2.add(sp);
	}
	
	public void addButonInsertListener(ActionListener e) {
		butonInsert.addActionListener(e);
	}
	
	public void addButonUpdateListener(ActionListener e) {
		butonUpdate.addActionListener(e);
	}
	
	public void addButonDeleteListener(ActionListener e) {
		butonDelete.addActionListener(e);
	}

	
	public void showErrorMessageInsert() {
		JOptionPane.showMessageDialog(frame," Please fill all of the fields", "Error", JOptionPane.ERROR_MESSAGE);
	}
	
	public void showErrorMessageUpdate() {
		JOptionPane.showMessageDialog(frame," Please fill all of the fields", "Error", JOptionPane.ERROR_MESSAGE);
	}
	
	public void showErrorMessageDelete() {
		JOptionPane.showMessageDialog(frame," Please fill the id field", "Error", JOptionPane.ERROR_MESSAGE);
	}
	
	public void showErrorMessageInsertExistAlready() {
		JOptionPane.showMessageDialog(frame,"Primary key id violated", "Error", JOptionPane.ERROR_MESSAGE);
	}
	
	public void showErrorMessageeInvalidId() {
		JOptionPane.showMessageDialog(frame,"id doesn't exist", "Error", JOptionPane.ERROR_MESSAGE);
	}
	
	public void showErrorMessageeInvalidIdParse() {
		JOptionPane.showMessageDialog(frame,"id error", "Error", JOptionPane.ERROR_MESSAGE);
	}
	
}
