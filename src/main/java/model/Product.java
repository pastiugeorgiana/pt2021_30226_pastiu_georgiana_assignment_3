package model;

public class Product {
	
	private int barcode;
	private String denumire;
	private int stock;
	private float price;

	
	public Product(int barcode, String denumire, int stock, float price) {
		super();
		this.barcode = barcode;
		this.denumire = denumire;
		this.stock = stock;
		this.price = price;

	}
	
	public Product() {
		
	}
	
	public int getBarcode() {
		return barcode;
	}
	public void setBarcode(int barcode) {
		this.barcode = barcode;
	}
	public String getDenumire() {
		return denumire;
	}
	public void setDenumire(String denumire) {
		this.denumire = denumire;
	}
	public int getStock() {
		return stock;
	}
	public void setStock(int stock) {
		this.stock = stock;
	}
	public float getPrice() {
		return price;
	}
	public void setPrice(float price) {
		this.price = price;
	}

	public String toString() {
		return "Product id: " + this.barcode + " name: " + this.denumire + " in stock: " + this.stock + " produced by " ;
	}
	
}
