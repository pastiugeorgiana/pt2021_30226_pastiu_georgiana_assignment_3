package model;

public class Client {

	private int id;
	private String name;
	private String address;
	private String prenume;
	private String email;
	
	public Client(int id, String name,  String prenume,String address, String email) {
		super();
		this.id = id;
		this.name = name;
		this.address = address;
		this.prenume = prenume;
		this.email = email;
	}
	
	public Client() {
		
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getPrenume() {
		return prenume;
	}
	public void setPrenume(String p) {
		this.prenume = p;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}

	public String toString() {
		return    "client data: id" + this.id + " nume" + this.name + "prenume " + this.prenume + " adresa" + this.address + "email " + this.email;
	}
	
}
