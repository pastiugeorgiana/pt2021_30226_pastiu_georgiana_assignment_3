package model;

public class Invoice {
	
	private int no;
	private int item;
	private int pieces;
	private int clientId;
	
	public Invoice(int item, int pieces, int clientId ) {
		this.item = item;
		this.pieces = pieces;
		this.clientId = clientId;
	}
	
	public Invoice() {
		super();
	}

	public int getNo() {
		return no;
	}

	public void setNo(int no) {
		this.no = no;
	}

	public int getItem() {
		return item;
	}

	public void setItem(int item) {
		this.item = item;
	}

	public int getPieces() {
		return pieces;
	}

	public void setPieces(int pieces) {
		this.pieces = pieces;
	}

	public int getClientId() {
		return clientId;
	}

	public void setClientId(int clientId) {
		this.clientId = clientId;
	}
	
	public String toString() {
		return "S-a emis factura cu numarul: "+this.no+ "pentru clientul cu id :"+ this.clientId + " la produsul: " + this.item + " din care s-au cerut " + this.pieces+"bucati";
	}
	
}
