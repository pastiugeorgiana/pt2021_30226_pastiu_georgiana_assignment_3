package model;
public class Orderr {

	private int Inumber;
	private int clientId;
	private String product;
	private float price;
	
	public Orderr(int clientId, String product, float price) {

		this.clientId = clientId;
		this.product = product;
		this.price = price;
	}
	
	public Orderr() {
		
	}
	
	public int getInumber() {
		return Inumber;
	}
	public void setInumber(int inumber) {
		this.Inumber = inumber;
	}
	public int getClientId() {
		return clientId;
	}
	public void setClientId(int clientId) {
		this.clientId = clientId;
	}
	public float getPrice() {
		return price;
	}
	public void setPrice(float price) {
		this.price = price;
	}
	public String getProduct() {
		return product;
	}
	public void setProduct(String product) {
		this.product = product;
	}
	
	public String toString() {
		return "Comanda: clientul: " + this.clientId + " comanda " + this.product + " suma de plata " + this.price;
	}

}
